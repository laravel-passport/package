<?php

namespace Pirago\Oauth2\Providers;

use Illuminate\Support\ServiceProvider;

class Oauth2Provider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->publishes([
            __DIR__.'/../config/client.php' => config_path('pirago_client.php'),
        ]);
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/client.php', 'pirago_client');
    }
}