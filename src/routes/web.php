<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'as' => 'pirago.oauth2.',
    'prefix' => '/oauth',
    'namespace' => '\Pirago\Oauth2\Controllers'
], function () {
    Route::post('/login', [
        'name' => 'login',
        'uses' => 'AuthController@login',
    ]);
});
