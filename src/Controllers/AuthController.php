<?php

namespace Pirago\Oauth2\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AuthController
{
    public $client;

    private string $baseUrl;

    private string $clientId;

    private string $clientSecret;

    private string $redirectUri;

    public function __construct()
    {
        $this->baseUrl = config("pirago_client.client_auth");
        $this->clientId = config("pirago_client.client_id");
        $this->clientSecret = config("pirago_client.client_secret");
        $this->redirectUri = config("pirago_client.client_redirect");

        $this->client = Http::baseUrl($this->baseUrl);
    }

    public function login(string $code)
    {
        try {
            $response = $this->client->asForm()->post('/oauth/token', [
                'grant_type' => 'authorization_code',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'redirect_uri' => $this->redirectUri,
                'code' => $code
            ]);

            return $response->json();
        } catch (\Throwable $e) {
            Log::error($e);
            throw $e;
        }
    }
}