<?php

return [
    'client_auth'       => env('AUTH_CLIENT'),
    'client_name'       => env('PASSPORT_CLIENT_NAME'),
    'client_id'         => env('PASSPORD_CLIENT_ID'),
    'client_secret'     => env('PASSPORT_CLIENT_SECRET'),
    'client_redirect'   => env('PASSPORT_CLIENT_REDIRECT'),
];
